import { defineConfig } from "astro/config";
import relativeLinks from "astro-relative-links";
import mkcert from "vite-plugin-mkcert";

import robotsTxt from "astro-robots-txt";
import sitemap from "@astrojs/sitemap";
import sentry from "@sentry/astro";
import spotlightjs from "@spotlightjs/astro";

// https://astro.build/config
export default defineConfig({
	root: "",
	outDir: "./dist",
	build: {
		assets: "assets",
		assetsPrefix: ".",
	},
	site: "https://www.websiteurl.com/",
	output: "static",
	integrations: [
		relativeLinks(),
		sentry(),
		spotlightjs(),
		robotsTxt({
			policy: [
				{
					userAgent: "*",
					allow: "/",
				},
				{
					userAgent: "AdsBot-Google",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "Amazonbot",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "anthropic-ai",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "AwarioRssBot",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "AwarioSmartBot",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "Bytespider",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "CCBot",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "ChatGPT-User",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "ClaudeBot",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "Claude-Web",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "cohere-ai",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "DataForSeoBot",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "FacebookBot",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "Google-Extended",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "GPTBot",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "ImagesiftBot",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "magpie-crawler",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "omgili",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "omgilibot",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "peer39_crawler",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "peer39_crawler/1.0",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "PerplexityBot",
					disallow: "/",
					crawlDelay: 2,
				},
				{
					userAgent: "YouBot",
					disallow: "/",
					crawlDelay: 2,
				},
			],
			sitemapBaseFileName: "sitemap-index",
			sitemap: ["https://www.studiogalion.com/sitemap-index.xml"],
		}),
		sitemap(),
	],
	vite: {
		plugins: [mkcert()],
		server: {
			https: true,
		},
		css: {
			preprocessorOptions: {
				css: {
					additionalData: '@import "./node_modules/modern-normalize/modern-normalize.css";',
				},
				scss: {
					additionalData: '@import "./src/sass/main.scss";',
				},
			},
		},
	},
});