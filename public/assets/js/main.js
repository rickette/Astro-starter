document.addEventListener("DOMContentLoaded", () => {
    console.log("main.js chargé");

    const breakpoints = [(portrait = 600), (landscape = 900), (desktop = 1200), (large = 1800)];
    const bpMobile = 900;
    const isMobile = window.innerWidth <= breakpoints.landscape;
    //console.log("isMobile : " + isMobile);

    // DEBUG MODE
    const debugToggle = document.querySelector("#debug-wrapper .toggle");
    const bodySelector = document.querySelector("#body");

    debugToggle.addEventListener("click", () => {
        bodySelector.querySelector("input").checked ? bodySelector.classList.add("debug") : bodySelector.classList.remove("debug");
    });

    // BACK TO TOP
    const onscroll = (el, listener) => {
        el.addEventListener("scroll", listener);
    };

    let backtotop = document.querySelector("#back-to-top");
    if (backtotop) {
        const toggleBacktotop = () => {
            if (window.scrollY > 100) {
                backtotop.classList.add("active");
            } else {
                backtotop.classList.remove("active");
            }
        };
        window.addEventListener("load", toggleBacktotop);
        onscroll(document, toggleBacktotop);
    }

    /*// MOBILE NAVIGATION // */
    /*////////////////////////*/
    if (isMobile) {
        const documentBody = document.querySelector("#document-body");
        const navToggle = document.querySelector("#mobile-nav-toggle");
        const navigation = document.querySelector("#navigation");
        const menuItem = document.querySelectorAll("#navigation .menu-item a");

        function toggleMobileNavigation() {
            documentBody.toggleAttribute("data-overlay");
            navigation.toggleAttribute("data-visible");
        }

        navToggle.addEventListener("click", () => {
            // loads mobile nav if not previously opened, adds overlay and change logo color
            navigation.hasAttribute("data-visible")
                ? (navToggle.setAttribute("data-expanded", false), navigation.setAttribute("data-expanded", false))
                : (navToggle.setAttribute("data-expanded", true), navigation.setAttribute("data-expanded", true));
            documentBody.toggleAttribute("data-overlay");
            navigation.toggleAttribute("data-visible");
        });
        menuItem.forEach((el) => {
            el.addEventListener("click", () => {
                toggleMobileNavigation();
            });
        });
    }
});
